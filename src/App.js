import React, { Component } from 'react';
import './App.css';
import AddTaskForm from './task-2/ToDo/AddTaskForm';
import Task from "./task-2/ToDo/Task";

class App extends Component {

    state = {
        tasks: [],
        textInput: '',
        textInputPrice: '',
        totalPrice: 0
    };

    addTask = () => {
        if (this.state.textInput !== '') {
            const tasks =  [...this.state.tasks];
            const newTask = {
                text: this.state.textInput,
                price: this.state.textInputPrice,
                id: Date.now()
            };

            tasks.push(newTask);

            let totalPrice = tasks.reduce((total, item) => total + parseInt(item.price), 0);

            this.setState({tasks, totalPrice});
        }
    };

    removeItems = id => {
        const tasks = [...this.state.tasks];
        const index = tasks.findIndex(task => {
            return (
                task.id === id
            )
        });
        tasks.splice(index, 1);
        let totalPrice = tasks.reduce((total, item) => total + parseInt(item.price), 0);
        this.setState({tasks, totalPrice})
    };

    changeHandler = (event) => {
        this.setState({
            textInput: event.target.value
        })
    };
    changeHandlerCost = (event) => {
        this.setState({
            textInputPrice: event.target.value
        })
    };

    render() {
        return (
            <div className="App">
                <AddTaskForm price={(event) => this.changeHandlerCost(event)}
                             change={(event) => this.changeHandler(event)}
                             add={() => this.addTask()}/>
                <div className='tasks'>
                    <Task entries = {this.state.tasks}
                          remove = {(id) => this.removeItems(id)}
                    />
                    Total spent: {this.state.totalPrice}
                </div>
            </div>
        );
    }
}

export default App;
