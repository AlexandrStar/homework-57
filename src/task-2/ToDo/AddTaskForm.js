import React from 'react';
import './ToDo.css';

const AddTaskForm = (props) => {
        return (
            <div>
            <div className='task-form'>
                <input onChange={props.change} className='task-input' type="text" placeholder="Item name" />
                <input onChange={props.price} className='task-kgs' type="text" placeholder="Cost" />
                <p className='kgs'>KGS</p>
                <button onClick={props.add} className='btn-add'>Add</button>
            </div>
            </div>
        )
};


export default AddTaskForm;