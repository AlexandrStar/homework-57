import React from 'react';
import './ToDo.css';

const Task = (props) =>  {

        return (
            props.entries.map((task) => {
                return (
                    <div className='task' key={task.id}>
                        <p>{task.text}</p>
                        <p>{task.price}KGS</p>
                        <button onClick={props.remove} className='btn-remove'>Remove</button>
                    </div>
                )
            })
        )

};

export default Task;