const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

const totalCountTimeToFrontend = tasks.reduce((acc, item) => {
    if (item.category === 'Frontend') {
        acc += item.timeSpent;
    }
    return acc;
}, 0);

const totalCountTimeToBug = tasks.reduce((acc, item) => {
    if (item.type === 'bug') {
        acc += item.timeSpent;
    }
    return acc;
}, 0);

const countVall = tasks.filter(item => item.title.includes('UI')).length;

const numberOfTasksInEachCategory = tasks.reduce((acc, item) => {
    const category = item.category;
    if (category in acc) {
        acc[category]++;
    } else {
        acc[category] = 1;
    }
    return acc;
}, {});

const arrayTask = tasks.map(item => {
    if (item.timeSpent > 4) {
        return {title: item.title, category: item.category}
    }
}).filter(item => item !== undefined);

console.log('Общее количество времени, затраченное на работу над задачами из категории "Frontend" - ' + totalCountTimeToFrontend);

console.log('Общее количество времени, затраченное на работу над задачами типа "bug" - ' + totalCountTimeToBug);

console.log('Количество задач, имеющих в названии слово "UI" - ' + countVall);

console.log('Количество задач каждой категории "Frontend" и "Backend" - ', numberOfTasksInEachCategory);

console.log('Массив задач с затраченным временем больше 4 часов - ', arrayTask);